package com.example.postdemo.ui.post

import com.example.postdemo.domain.post.PostInteractor
import com.example.postdemo.ui.model.PostItem
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.orbitmvi.orbit.SuspendingTestContainerHost
import org.orbitmvi.orbit.test

@ExperimentalCoroutinesApi
class PostViewModelTest {

    private lateinit var postViewModelTestSubject: SuspendingTestContainerHost<PostState, PostSideEffect, PostViewModel>

    @MockK(relaxed = true)
    lateinit var postInteractor: PostInteractor

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        postViewModelTestSubject = PostViewModel(postInteractor).test(PostState())
    }

    @Test
    fun `fetchAllPosts sets the PostState with correct results`() = runTest {
        postViewModelTestSubject.runOnCreate()

        val result = PostResult.Post(listOf(PostItem("1", "body", "title", "2")))
        every { postInteractor.fetchAllPosts() } returns flowOf(result)

        postViewModelTestSubject.testIntent { fetchPosts() }

        postViewModelTestSubject.assert(PostState()) {
            states(
                { copy(posts = result.posts, isLoading = false, isError = false) }
            )
        }
    }

    @Test
    fun `fetchAllPosts sets the error when error result is received`() = runTest {
        postViewModelTestSubject.runOnCreate()

        every { postInteractor.fetchAllPosts() } returns flowOf(PostResult.Error(RuntimeException()))

        postViewModelTestSubject.testIntent { fetchPosts() }

        postViewModelTestSubject.assert(PostState()) {
            states(
                { copy(isLoading = false, isError = true) }
            )
        }
    }
}