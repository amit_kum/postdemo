package com.example.postdemo.ui.post

import com.example.postdemo.domain.post.PostInteractor
import com.example.postdemo.ui.model.PostItemDetail
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.orbitmvi.orbit.SuspendingTestContainerHost
import org.orbitmvi.orbit.test

@ExperimentalCoroutinesApi
class PostDetailViewModelTestprivate {
    lateinit var postDetailViewModelTestSubject: SuspendingTestContainerHost<PostDetailState, PostSideEffect, PostDetailViewModel>


    @MockK(relaxed = true)
    lateinit var postInteractor: PostInteractor

    @MockK(relaxed = true)
    lateinit var mockPostDetail: PostDetailResult.PostDetail

    @MockK
    lateinit var mockPostItemDetail: PostItemDetail

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        postDetailViewModelTestSubject = PostDetailViewModel(postInteractor).test(PostDetailState())
    }

    @Test
    fun `fetchPostItemDetail sets the PostDetailState with correct results`() = runTest {
        postDetailViewModelTestSubject.runOnCreate()

        every { postInteractor.fetchPostItemDetail(any(), any()) } returns flowOf(mockPostDetail)

        every { mockPostDetail.postItemDetail } returns mockPostItemDetail

        postDetailViewModelTestSubject.testIntent { fetchPost("postId", "userId") }

        postDetailViewModelTestSubject.assert(PostDetailState()) {
            states(
                {
                    copy(
                        postItemDetail = mockPostDetail.postItemDetail,
                        isLoading = false,
                        isError = false
                    )
                }
            )
        }
    }

    @Test
    fun `fetchPostItemDetail sets the error when error result is received`() = runTest {
        postDetailViewModelTestSubject.runOnCreate()

        every {
            postInteractor.fetchPostItemDetail(
                any(),
                any()
            )
        } returns flowOf(PostDetailResult.Error(RuntimeException()))

        postDetailViewModelTestSubject.testIntent { fetchPost("postId", "userId") }

        postDetailViewModelTestSubject.assert(PostDetailState()) {
            states(
                { copy(isLoading = false, isError = true) }
            )
        }
    }

}


