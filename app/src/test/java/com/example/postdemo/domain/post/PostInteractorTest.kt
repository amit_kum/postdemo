package com.example.postdemo.domain.post

import app.cash.turbine.test
import com.example.postdemo.CoroutineTestRule
import com.example.postdemo.api.model.Post
import com.example.postdemo.api.model.User
import com.example.postdemo.domain.user.UserRepository
import com.example.postdemo.ui.model.PostItem
import com.example.postdemo.ui.model.PostItemDetail
import com.example.postdemo.ui.post.PostDetailResult
import com.example.postdemo.ui.post.PostResult
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertSame
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class PostInteractorTest {

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @MockK(relaxed = true)
    lateinit var userRepository: UserRepository

    @MockK(relaxed = true)
    lateinit var postRepository: PostRepository

    lateinit var postInteractor: PostInteractor

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        postInteractor =
            PostInteractor(postRepository, userRepository, coroutineTestRule.testDispatcher)
    }

    @Test
    fun `fetchAllPosts starts with Loading and updates the values for successful scenario`() =
        runTest {
            coEvery { postRepository.fetchPosts() } returns getPostList()
            postInteractor.fetchAllPosts().test {
                assertEquals(PostResult.Loading, awaitItem())
                assertEquals(PostResult.Post(postItemList()), awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }

    @Test
    fun `fetchAllPosts send error state if the network call fails`() =
        runTest {
            coEvery { postRepository.fetchPosts() } throws RuntimeException()
            postInteractor.fetchAllPosts().test {
                assertEquals(PostResult.Loading, awaitItem())
                assertSame(PostResult.Error::class.java, awaitItem()::class.java,)
                cancelAndIgnoreRemainingEvents()
            }
        }

    @Test
    fun `fetchPostItemDetail starts with Loading and updates the values for successful scenario`() =
        runTest {
            coEvery { postRepository.fetchPost(any()) } returns getPost()
            coEvery { userRepository.fetchUser(any()) } returns getUser()

            postInteractor.fetchPostItemDetail("userId", "postId").test {
                assertEquals(PostDetailResult.Loading, awaitItem())
                assertEquals(PostDetailResult.PostDetail(getPostItemDetail()), awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }

    @Test
    fun `fetchPostItemDetail send error state if the network call fails for getPost`() =
        runTest {
            coEvery { postRepository.fetchPost(any()) } throws RuntimeException()

            postInteractor.fetchPostItemDetail("userId", "postId").test {
                assertEquals(PostDetailResult.Loading, awaitItem())
                assertSame(PostDetailResult.Error::class.java, awaitItem()::class.java,)
                cancelAndIgnoreRemainingEvents()
            }
        }

    @Test
    fun `fetchPostItemDetail send error state if the network call fails for getUser`() =
        runTest {
            coEvery { userRepository.fetchUser(any()) } throws RuntimeException()
            postInteractor.fetchPostItemDetail("userId", "postId").test {
                assertEquals(PostDetailResult.Loading, awaitItem())
                assertSame(PostDetailResult.Error::class.java, awaitItem()::class.java,)
                cancelAndIgnoreRemainingEvents()
            }
        }

    private fun getPost() = Post("1", "body", "title", "2")

    private fun getUser() = User("1", "name", "userNme")

    private fun getPostItemDetail(): PostItemDetail {
        val user = getUser()
        val post = getPost()

        return PostItemDetail(
            postId = post.id, userId = user.id,
            title = post.title, body = post.body,
            user = user.name, userName = user.username
        )
    }

    private fun getPostList() = listOf(getPost())

    private fun postItemList(): List<PostItem> {
        return getPostList().map {
            PostItem(id = it.id, body = it.body, title = it.title, userId = it.userId)
        }
    }
}