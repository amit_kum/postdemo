package com.example.postdemo.domain.user

import com.example.postdemo.api.webservice.UserService
import io.mockk.MockKAnnotations
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class UserRepositoryTest {

    @MockK(relaxed = true)
    lateinit var userService: UserService

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `fetchUser invokes userService fetchUserById call`() = runTest {
        val userRepository = UserRepository(userService)
        userRepository.fetchUser("someUserId")
        coVerify { userService.fetchUserById(any()) }
    }
}
