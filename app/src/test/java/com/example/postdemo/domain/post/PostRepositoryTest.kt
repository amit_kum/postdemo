package com.example.postdemo.domain.post

import com.example.postdemo.api.webservice.PostService
import io.mockk.MockKAnnotations
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class PostRepositoryTest {

    @MockK(relaxed = true)
    lateinit var postService: PostService

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `fetchPost invokes fetchPosts call on PostService`() = runTest {
        val postRepository = PostRepository(postService)
        postRepository.fetchPosts()

        coVerify { postService.fetchPosts() }
    }

    @Test
    fun `fetchPost invokes fetchPost call on PostService`() = runTest {
        val postRepository = PostRepository(postService)
        postRepository.fetchPost("somePostId")

        coVerify { postService.fetchPost(any()) }
    }

}