package com.example.postdemo.ui.post

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.postdemo.databinding.LayoutPostItemBinding
import com.example.postdemo.ui.model.PostItem

class PostListAdapter(
    private val postItemClickListener: PostItemClickListener
) : ListAdapter<PostItem, PostListAdapter.ViewHolder>(PostItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutPostItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            postItemClickListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val postItem = getItem(position)
        holder.bind(postItem)
    }

    class ViewHolder constructor(
        private val binding: LayoutPostItemBinding,
        private val postItemClickListener: PostItemClickListener
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(postItem: PostItem) {
            binding.postItem = postItem

            binding.root.setOnClickListener {
                postItemClickListener.postItemClicked(postItem)
            }
            binding.executePendingBindings()
        }
    }
}

class PostItemDiffCallback : DiffUtil.ItemCallback<PostItem>() {
    override fun areItemsTheSame(oldItem: PostItem, newItem: PostItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: PostItem, newItem: PostItem): Boolean {
        return oldItem == newItem
    }
}

interface PostItemClickListener {
    fun postItemClicked(postItem: PostItem)
}