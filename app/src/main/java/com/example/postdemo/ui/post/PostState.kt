package com.example.postdemo.ui.post

import com.example.postdemo.ui.model.PostItem
import com.example.postdemo.ui.model.PostItemDetail

data class PostState(
    val posts: List<PostItem> = emptyList(),
    val isLoading: Boolean = false,
    val isError: Boolean = false
)

sealed class PostResult {
    object Loading : PostResult()
    data class Error(val throwable: Throwable) : PostResult()
    data class Post(val posts: List<PostItem>) : PostResult()
}


data class PostDetailState(
    val postItemDetail: PostItemDetail = PostItemDetail(),
    val isLoading: Boolean = false,
    val isError: Boolean = false
)

sealed class PostDetailResult {
    object Loading : PostDetailResult()
    data class Error(val throwable: Throwable) : PostDetailResult()
    data class PostDetail(val postItemDetail: PostItemDetail) : PostDetailResult()
}

sealed class PostSideEffect