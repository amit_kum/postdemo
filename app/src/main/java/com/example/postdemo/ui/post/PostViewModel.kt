package com.example.postdemo.ui.post

import androidx.lifecycle.ViewModel
import com.example.postdemo.domain.post.PostInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
class PostViewModel @Inject constructor(
    private val postInteractor: PostInteractor
) : ContainerHost<PostState, PostSideEffect>, ViewModel() {

    override val container = container<PostState, PostSideEffect>(PostState())

    fun fetchPosts() = intent {
        postInteractor.fetchAllPosts().collect { postResult ->
            reduce {
                when (postResult) {
                    PostResult.Loading -> state.copy(isLoading = true)
                    is PostResult.Post -> state.copy(
                        posts = postResult.posts,
                        isLoading = false,
                        isError = false
                    )
                    is PostResult.Error -> state.copy(isError = true, isLoading = false)
                }
            }
        }
    }
}