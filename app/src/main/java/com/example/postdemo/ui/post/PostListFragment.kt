package com.example.postdemo.ui.post

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.example.postdemo.databinding.FragmentPostListBinding
import com.example.postdemo.ui.model.PostItem
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class PostListFragment : Fragment() {

    private lateinit var adapter: PostListAdapter
    private val viewModel: PostViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentPostListBinding.inflate(inflater)

        binding.errorLayout.retryButton.setOnClickListener {
            viewModel.fetchPosts()
        }

        adapter = PostListAdapter(object : PostItemClickListener {
            override fun postItemClicked(postItem: PostItem) {
                val action = PostListFragmentDirections.actionPostListFragmentToPostDetailFragment(
                    postId = postItem.id,
                    userId = postItem.userId
                )
                findNavController().navigate(action)
            }
        })

        viewModel.fetchPosts()

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.container.stateFlow.collect {
                    binding.showError = it.isError
                    binding.loading = it.isLoading
                    adapter.submitList(it.posts)
                }
            }
        }

        binding.list.adapter = adapter
        return binding.root
    }

}