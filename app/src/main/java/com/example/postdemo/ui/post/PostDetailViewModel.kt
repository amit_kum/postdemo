package com.example.postdemo.ui.post

import androidx.lifecycle.ViewModel
import com.example.postdemo.domain.post.PostInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
class PostDetailViewModel @Inject constructor(
    private val postInteractor: PostInteractor
) : ContainerHost<PostDetailState, PostSideEffect>, ViewModel() {

    override val container = container<PostDetailState, PostSideEffect>(PostDetailState())

    fun fetchPost(postId: String, userId: String) = intent {
        postInteractor.fetchPostItemDetail(postId = postId, userId = userId)
            .collect { postDetailResult ->
                reduce {
                    when (postDetailResult) {
                        PostDetailResult.Loading -> state.copy(isLoading = true)
                        is PostDetailResult.PostDetail -> state.copy(
                            postItemDetail = postDetailResult.postItemDetail,
                            isLoading = false,
                            isError = false
                        )
                        is PostDetailResult.Error -> state.copy(isError = true, isLoading = false)
                    }
                }
            }
    }
}