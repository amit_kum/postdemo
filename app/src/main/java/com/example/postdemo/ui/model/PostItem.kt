package com.example.postdemo.ui.model

data class PostItem(
    val id: String,
    val body: String,
    val title: String,
    val userId: String
)