package com.example.postdemo.ui.model

data class PostItemDetail(
    val postId: String = "",
    val userId: String = "",
    val body: String = "",
    val title: String = "",
    val user: String = "",
    val userName: String = ""
)