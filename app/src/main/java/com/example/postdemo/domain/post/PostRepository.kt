package com.example.postdemo.domain.post

import com.example.postdemo.api.model.Post
import com.example.postdemo.api.webservice.PostService
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val postService: PostService
) {

    suspend fun fetchPosts(): List<Post> {
        return postService.fetchPosts()
    }

    suspend fun fetchPost(postId: String): Post {
        return postService.fetchPost(postId)
    }
}