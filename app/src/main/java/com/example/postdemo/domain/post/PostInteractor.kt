package com.example.postdemo.domain.post

import com.example.postdemo.domain.user.UserRepository
import com.example.postdemo.ui.model.PostItem
import com.example.postdemo.ui.model.PostItemDetail
import com.example.postdemo.ui.post.PostDetailResult
import com.example.postdemo.ui.post.PostResult
import com.google.samples.apps.iosched.shared.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

class PostInteractor @Inject constructor(
    private val postRepository: PostRepository,
    private val userRepository: UserRepository,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) {
    fun fetchAllPosts() = flow<PostResult> {
        val postItems = postRepository.fetchPosts().map {
            PostItem(id = it.id, body = it.body, title = it.title, userId = it.userId)
        }
        emit(PostResult.Post(postItems))
    }
        .onStart { emit(PostResult.Loading) }
        .flowOn(dispatcher)
        .catch { emit(PostResult.Error(it)) }

    fun fetchPostItemDetail(postId: String, userId: String) = flow<PostDetailResult> {
        val user = userRepository.fetchUser(userId)
        val post = postRepository.fetchPost(postId)

        val postItemDetail = PostItemDetail(
            postId = post.id, userId = user.id,
            title = post.title, body = post.body,
            user = user.name, userName = user.username
        )

        emit(PostDetailResult.PostDetail(postItemDetail))
    }
        .onStart { emit(PostDetailResult.Loading) }
        .flowOn(dispatcher)
        .catch { emit(PostDetailResult.Error(it)) }
}