package com.example.postdemo.domain.user

import com.example.postdemo.api.model.User
import com.example.postdemo.api.webservice.UserService
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val userService: UserService
) {

    suspend fun fetchUser(userId: String): User {
        return userService.fetchUserById(userId)
    }
}