package com.example.postdemo.api.model

data class Post(
    val id: String,
    val body: String,
    val title: String,
    val userId: String
)