package com.example.postdemo.api.webservice

import com.example.postdemo.api.model.User
import retrofit2.http.GET
import retrofit2.http.Path

interface UserService {
    @GET("users/{userId}")
    suspend fun fetchUserById(@Path("userId") userId: String): User
}