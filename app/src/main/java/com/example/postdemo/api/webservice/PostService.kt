package com.example.postdemo.api.webservice

import com.example.postdemo.api.model.Post
import retrofit2.http.GET
import retrofit2.http.Path

interface PostService {
    @GET("posts")
    suspend fun fetchPosts(): List<Post>

    @GET("posts/{postId}")
    suspend fun fetchPost(@Path("postId") postId: String): Post
}