package com.example.postdemo.api.model

data class User(
    val id: String,
    val name: String,
    val username: String
)